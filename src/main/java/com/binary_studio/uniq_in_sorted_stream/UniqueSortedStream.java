package com.binary_studio.uniq_in_sorted_stream;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

public final class UniqueSortedStream {

	private UniqueSortedStream() {
	}

	public static <T> Stream<Row<T>> uniqueRowsSortedByPK(Stream<Row<T>> stream) {
		Stream<Row<T>> result = stream.filter(distinctById(Row::getPrimaryId));
		return result;
	}

	public static <T> Predicate<T> distinctById(Function<? super T, ?> keyExtractor) {
		Set<Object> ids = ConcurrentHashMap.newKeySet();
		return v -> ids.add(keyExtractor.apply(v));
	}
}
