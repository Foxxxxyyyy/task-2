package com.binary_studio.tree_max_depth;

public final class DepartmentMaxDepth {
	Integer height = 0;

	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		int height = 0;
		if (rootDepartment == null) {
			return 0;
		}
		else {
			if (rootDepartment.getSubDepartments() != null) {
				if (rootDepartment.getSubDepartments().size() != 0) {
					for (int i = 0; i < rootDepartment.subDepartments.size(); i++) {
						height = Math.max(height, calculateMaxDepth(rootDepartment.subDepartments.get(i)));
					}
				}
				return height + 1;
			}
			else {
				return height;
			}
		}
	}
}


