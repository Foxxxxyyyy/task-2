package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl implements AttackSubsystem {


	private PositiveInteger powerGridConsumption;
	private String name;
	private PositiveInteger cpConsumption;
	private PositiveInteger optimalSize;

	public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {

		if (name.isBlank()  || name == null) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		AttackSubsystemImpl.construct(name, powergridRequirments, capacitorConsumption, optimalSpeed, optimalSize, baseDamage);
		AttackSubsystemImpl syst = AttackSubsystemBuilder.named(name)
				.optimalSize(optimalSize.value())
				.optimalSpeed(optimalSpeed.value())
				.damage(baseDamage.value())
				.pg(powergridRequirments.value())
				.capacitorUsage(capacitorConsumption.value())
				.construct();


		syst.setName(name);
		syst.setCPConsumption(capacitorConsumption);
		syst.setOptimalSize(optimalSize);
		syst.setPowerGridConsumption(powergridRequirments);

		return syst;
	}

	public void setPowerGridConsumption(PositiveInteger powerGridConsumption) {
		this.powerGridConsumption = powerGridConsumption;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setCPConsumption(PositiveInteger cpConsumption) {
		this.cpConsumption = cpConsumption;
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powerGridConsumption;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.cpConsumption;
	}

	@Override
	public PositiveInteger attack(Attackable target) {
		Integer result = 1;
		if (target.getSize().value() < this.getOptimalSize().value()) {
			result = target.getSize().value() / this.getOptimalSize().value();
		}
		return new PositiveInteger(result);
	}

	@Override
	public String getName() {
		return this.name;
	}

	public PositiveInteger getOptimalSize() {
		return this.optimalSize;
	}

	public void setOptimalSize(PositiveInteger optimalSize) {
		this.optimalSize = optimalSize;
	}

}
