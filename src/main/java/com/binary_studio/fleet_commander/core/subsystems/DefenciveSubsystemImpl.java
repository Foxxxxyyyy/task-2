package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DefenciveSubsystemImpl implements DefenciveSubsystem {
	private String name;
	private PositiveInteger pwCons;
	private PositiveInteger cpCons;
	private PositiveInteger impRedPer;
	private PositiveInteger shldRegen;
	private PositiveInteger hullRegen;


	public static DefenciveSubsystemImpl construct(String name, PositiveInteger powergridConsumption,
			PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
			PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) throws IllegalArgumentException {

		if (name.isBlank()  || name == null) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}

		DefenciveSubsystemImpl deff = DefenciveSubsystemBuilder.named(name)
				.pg(powergridConsumption.value())
				.impactReduction(impactReductionPercent.value())
				.shieldRegen(shieldRegeneration.value())
				.hullRegen(hullRegeneration.value())
				.construct();

		deff.setCpCons(capacitorConsumption);
		deff.setPwCons(powergridConsumption);
		deff.setName(name);

		return deff;
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.pwCons;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.cpCons;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public AttackAction reduceDamage(AttackAction incomingDamage) {
		return null;
	}

	@Override
	public RegenerateAction regenerate() {
		this.hullRegen = new PositiveInteger(100);
		return null;
	}

	public void setPwCons(PositiveInteger pwCons) {
		this.pwCons = pwCons;
	}

	public void setCpCons(PositiveInteger cpCons) {
		this.cpCons = cpCons;
	}

	public void setName(String name) {
		this.name = name;
	}

}
